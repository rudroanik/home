package nipon.com.recyclerviewusingmvvmandretrofit.Repository;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.widget.Toast;

import java.util.List;

import nipon.com.recyclerviewusingmvvmandretrofit.Model.Post;
import nipon.com.recyclerviewusingmvvmandretrofit.Retrofit.APIService;
import nipon.com.recyclerviewusingmvvmandretrofit.Retrofit.RetrofitInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class PostRepository {
    private MutableLiveData<List<Post>> postList;
    private MutableLiveData<Boolean> isLoaded;
    private Application application;

    public PostRepository(Application application) {
        this.application = application;
        getAllPostsFromDB();
    }


    public MutableLiveData<List<Post>> getAllPost() {
        return postList;

    }

    public MutableLiveData<Boolean> getIsLoaded() {
        return isLoaded;
    }


    private void getAllPostsFromDB() {
        postList = new MutableLiveData<>();
        isLoaded = new MutableLiveData<>();

        Retrofit retrofit = RetrofitInstance.getRetrofitInstance();
        APIService apiService = retrofit.create(APIService.class);

        Call<List<Post>> call = apiService.getPosts();

        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                Toast.makeText(application, "OnResponse", Toast.LENGTH_SHORT).show();
                List<Post> posts = response.body();
                postList.setValue(posts);
                isLoaded.setValue(true);
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                Toast.makeText(application, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
