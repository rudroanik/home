package nipon.com.recyclerviewusingmvvmandretrofit.View.Activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import nipon.com.recyclerviewusingmvvmandretrofit.CustomAdapter.PostAdapter;
import nipon.com.recyclerviewusingmvvmandretrofit.Model.Post;
import nipon.com.recyclerviewusingmvvmandretrofit.R;
import nipon.com.recyclerviewusingmvvmandretrofit.ViewModel.PostViewModel;
import nipon.com.recyclerviewusingmvvmandretrofit.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    private PostAdapter postAdapter;
    private PostViewModel postViewModel;

    private List<Post> postList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        postViewModel = ViewModelProviders.of(this).get(PostViewModel.class);

        initRecyclerView();
        getAllPost();


    }

    private void getAllPost() {
        postViewModel.getAllPost().observe(this, new Observer<List<Post>>() {
            @Override
            public void onChanged(@Nullable List<Post> posts) {
                postList.clear();
                postList.addAll(posts);
                postAdapter.notifyDataSetChanged();
            }
        });
        getIsLoaded();
    }

    private void getIsLoaded() {
        postViewModel.getIsLoaded().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean==true){
                    binding.progressBar.setVisibility(View.GONE);
                }
            }
        });
    }

    private void initRecyclerView() {
        postList = new ArrayList<>();
        postAdapter = new PostAdapter(postList, this);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(postAdapter);
    }
}
