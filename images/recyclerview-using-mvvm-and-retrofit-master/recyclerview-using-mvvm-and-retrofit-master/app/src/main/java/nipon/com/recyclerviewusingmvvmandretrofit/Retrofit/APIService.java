package nipon.com.recyclerviewusingmvvmandretrofit.Retrofit;

import java.util.List;

import nipon.com.recyclerviewusingmvvmandretrofit.Model.Post;
import retrofit2.Call;
import retrofit2.http.GET;

public interface APIService {

    @GET("posts")
    Call<List<Post>> getPosts();
}
