package nipon.com.recyclerviewusingmvvmandretrofit.CustomAdapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import nipon.com.recyclerviewusingmvvmandretrofit.Model.Post;
import nipon.com.recyclerviewusingmvvmandretrofit.R;
import nipon.com.recyclerviewusingmvvmandretrofit.databinding.ItemPostBinding;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewModel> {
    private List<Post> postList;
    private Context context;

    public PostAdapter(List<Post> postList, Context context) {
        this.postList = postList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewModel onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        ItemPostBinding itemPostBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_post, viewGroup, false);
        return new ViewModel(itemPostBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewModel viewModel, int i) {
        Post post = postList.get(i);

        viewModel.binding.titleTV.setText(post.getTitle());
        viewModel.binding.bodyTV.setText(post.getBody());
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    public class ViewModel extends RecyclerView.ViewHolder {

        private final ItemPostBinding binding;

        public ViewModel(@NonNull ItemPostBinding itemPostBinding) {
            super(itemPostBinding.getRoot());
            this.binding = itemPostBinding;

        }
    }
}
